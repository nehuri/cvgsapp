# Virtual Game Store
ASP.NET MVC application for a game store website with some members-only and employees-only features.

## Technology Stack
ASP.NET, Entity Framework, C#, JavaScript, jQuery, AJAX, Bootstrap, HTML, CSS, MS SQL server

## License
For this project, an MIT license has been chosen due to its permissiveness - to encourage people to use our software and to contribute to the code development.

## Deployment Instructions
 
### Instal the Database
 
1.	Download the database SQL code.
2.	Open your Microsoft SQL Server Management Studio
3.	Open the downloaded sql file and run it - it will create a new database called CVGSApp with all the necessary tables, some of which (Provinces, GameCategories, GamePlatforms) will be prepopulated.

### How to Install and Configure the Web Site
 
1.	Download the application from a GitHub repository (you need to obtain permissions first) by opening a Git Bash prompt in the directory where you want to have your source code and running a command “git clone https://github.com/CaesarSaladGaming/CVGSApp.git”. This will create a Visual Studio solution called CVGSApp.
2.	Open the solution in Visual Studio.
3.	Modify the connection string to point to the database created in the section How to Create the Database:
	* Open file Web.config in the main project (CVGS)
	* In the line <add name="DefaultConnection" connectionString="Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=CVGSApp;Integrated Security=True" providerName="System.Data.SqlClient" /> replace “LocalDb)\MSSQLLocalDB” with the name of your database server
	* In the line <add name="CVGSAppEntities" connectionString="metadata=res://*/CVGSModel.csdl|res://*/CVGSModel.ssdl|res://*/CVGSModel.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=(LocalDB)\MSSQLLocalDB;initial catalog=CVGSApp;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />, replace all instances of “LocalDb)\MSSQLLocalDB” with the name of your database server
	* Open the file App.Config in the CVGS.Data project and repeat the step c.
4.	Build the solution (Ctrl+Shift+B).
5.	Run the application (F5).
6.	Create the first Admin user that will have access to the Administration section of the site:
	* Navigate to the Home page of the application by typing http://localhost:56061 in the address bar of the browser
	* Click on Register button and register a new user that will have administrative privileges
	* In the AspNetRoles data table, create a new role called Employee.
	* In the AspNetUserRoles data table, register the created user as being in the Employee role by adding a new record with user Id in the UserId field and the role Id in the RoleId field.
	* Navigate to the website and log in as the newly created user. In the navigation bar, you will see a new button named Administration.

### How to Run the Web Site
To access the web site with a browser, it is sufficient to type http://localhost:56061 in the address bar of the browser while the application is being run.

